# NixOS nix expression file
{ pkgs ? import <nixpkgs> {} }:

pkgs.python37Packages.buildPythonApplication rec {
  name = "wippy";
  propagatedBuildInputs = with pkgs; [
    glibcLocales
    pkgconfig
  ];

  preShellHook = ''
    unset SOURCE_DATE_EPOCH
  '';
}
