import os


class Node():
    """Wrapper for this program's tree nodes.

    Used in a filesystem tree with notes attached.

    Attributes:
        name str - File path.
        note str - String description of file path.
        children dict(str:Node) - str key is child name - used for easy
            searching during tree generation. Mostly unnecessary for basic
            traversal.
    """

    def __init__(self, name):
        self.name = name
        self.note = None
        self.children = {}

    def __str__(self, level=0):
        """String representation for pretty debugging."""
        ret = "\t" * level + f'{self.name}:{self.note}' + "\n"
        for child in self.children.values():
            ret += child.__str__(level + 1)
        return ret


def collapse_tree(tree):
    """
    Collapse the path of series of single child nodes.

    e.g.
    /:None
    home:None
        user:None
            devel:None
                fsnotetaking:None
                    foo:bar
                        baz:None
                            foobar:asdf
                                foofoo:None
                                    barbar:asdf
                                    bazbaz:asdf
                    bar:asfdaf

    Converts to:

    /home/user/devel/fsnotetaking:None
    foo:bar
        baz/foobar:asdf
            foofoo:None
                barbar:asdf
                bazbaz:asdf
    bar:asfdaf

    """
    if len(tree.children) == 0:
        return tree
    elif len(tree.children) == 1 and tree.note is None:
        child = next(iter(tree.children.values()))
        if tree.name == '/':
            tree.name = tree.name + child.name
        else:
            tree.name = tree.name + '/' + child.name
        tree.children = child.children
        tree.note = child.note
        return collapse_tree(tree)

    # Collapse each child, but return this node.
    for child in tree.children.values():
        collapse_tree(child)
    return tree


def generate_tree(notes):
    """
    Create a tree of Node's based on notes.
    """
    # TODO: Cross platform?
    output = Node('/')
    for path, note in notes:
        # Assume '/'
        parts = splitall(path)[1:]
        generate_tree_helper(output, parts, note)
    return output


def generate_tree_helper(accum, parts, note):
    if parts == []:
        accum.note = note
        return accum

    if parts[0] not in accum.children:
        accum.children[parts[0]] = Node(parts[0])
    return generate_tree_helper(accum.children[parts[0]], parts[1:], note)


def splitall(path):
    """
    Split path into component parts.

    /home/foo/devel/ -> ['/', 'home', 'foo', 'devel']
    """
    # Our database only contains absolute paths without trailing slashes.
    allparts = []
    while 1:
        parts = os.path.split(path)
        if parts[0] == path:   # sentinel for absolute paths
            allparts.insert(0, parts[0])
            break
        elif parts[1] == path:  # sentinel for relative paths
            allparts.insert(0, parts[1])
            break
        else:
            path = parts[0]
            allparts.insert(0, parts[1])
    return allparts
