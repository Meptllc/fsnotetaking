import sqlite3


class SqliteBackend:
    '''
    CRUD interface for a two column table.
    '''
    def __init__(self, filename='CHANGEME.db'):
        self.filename = filename
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''
            CREATE TABLE IF NOT EXISTS fsnotetaking_notes (
                     path TEXT UNIQUE,
                     note TEXT)
        ''')
        conn.commit()
        conn.close()

    def update_note(self, path, note):
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''
            INSERT OR REPLACE INTO fsnotetaking_notes VALUES(?, ?)
        ''', (path, note))
        conn.commit()
        conn.close()

    def get_note(self, path):
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''
            SELECT note FROM fsnotetaking_notes WHERE path = ?
        ''', (path,))
        item = c.fetchone()
        conn.close()
        return item

    def get_notes(self):
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''
            SELECT * FROM fsnotetaking_notes
        ''')
        items = c.fetchall()
        conn.close()
        return items

    def delete_note(self, path):
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''
            DELETE FROM fsnotetaking_notes WHERE path = ?
        ''', (path,))
        conn.commit()
        conn.close()

    def delete_notes(self):
        conn = sqlite3.connect(self.filename)
        c = conn.cursor()
        c.execute('''
            DELETE FROM fsnotetaking_notes
        ''')
        conn.commit()
        conn.close()
