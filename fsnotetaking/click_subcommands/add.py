import click
import os
import subprocess
import tempfile
from fsnotetaking.backend.sqlite import SqliteBackend


@click.command()
@click.argument('path')
@click.option('--message', '-m', help='The note. if not given open EDITOR.')
@click.option('--force', '-f', is_flag=True, help='Save note even if path is inaccessible or message is empty.')
def add(path, message, force):
    """Add/edit a note entry for a path."""
    path = os.path.abspath(path)
    if not force and not os.path.lexists(path):
        click.echo(f'File path {path} does not exist. Use --force to add a note anyway.')
        raise click.Abort()

    backend = SqliteBackend()
    current_note = backend.get_note(path)

    if message is None:
        # Open the current note in EDITOR.
        editor = os.getenv('EDITOR', 'vi')
        f = tempfile.NamedTemporaryFile(delete=False)
        if current_note is not None:
            f.write(bytes(current_note[0], 'utf8'))
        f.close()
        subprocess.run([editor, f.name])
        # Reopen the file to see the changes by EDITOR.
        with open(f.name) as f:
            message = f.read()
        os.unlink(f.name)

    update_note = True
    if message == '':
        if current_note is not None:
            print(f'Saving empty note. Use the delete subcommand to delete the note.')
        elif not force:
            update_note = False
            print(f'Empty note, entry for {path} not created. Use --force to add a note anyway.')

    if update_note:
        backend.update_note(path, message)
