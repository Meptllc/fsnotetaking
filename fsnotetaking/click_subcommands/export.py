import click
from fsnotetaking.exports.jira_expands import jira_expands
from fsnotetaking.backend.sqlite import SqliteBackend
from fsnotetaking import fstree


@click.command()
@click.option('--format', default='string', help='Output formats: jira.')
def export(format):
    """Export a note entry for a path."""
    backend = SqliteBackend()
    notes = backend.get_notes()
    if len(notes) == 0:
        click.echo('No notes exist.')
    else:
        structured_notes = fstree.generate_tree(notes)
        structured_notes = fstree.collapse_tree(structured_notes)
        if format == 'jira':
            print(jira_expands(structured_notes))
        else:
            print(structured_notes)
